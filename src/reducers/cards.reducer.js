import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    cards: [],
}

const cardsSlice = createSlice({
    name: "cards",
    initialState,
    reducers: {
        actionCards: (state, {payload}) =>{
            state.cards = [...payload];
        }
    }
});
export const {actionCards} = cardsSlice.actions;

export const actionFetchCards = () => (dispatch) =>{
    return fetch("phones.json")
    .then((response) => response.json())  
    .then((results)=>{
        console.log(results);
        dispatch(cardsSlice.actions.actionCards(results));
    })
}
export default cardsSlice.reducer;