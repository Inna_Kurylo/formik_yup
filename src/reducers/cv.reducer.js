import {createSlice } from "@reduxjs/toolkit";

const initialState = {
    cv: {
        name: "",
        soname: "",
        phone: "",
        address: "",
        email: "",
    }
}
const cvSlice = createSlice({
    name: "cv",
    initialState,
    reducers: {
        actionCv: (state, {payload}) =>{
            state.cv = {...payload};
        }
    }
})
export const {actionCv} = cvSlice.actions;
export default cvSlice.reducer;