import appReducer, {actionBuyAll, actionAddCart, actionRemoveCart, actionCurrentProd, actionModal, actionAddFavorites, actionRemoveFavorite} from "./app.reducer";
import cardsReducer, {actionFetchCards} from "./cards.reducer";
import cvReducer, {actionCv} from "./cv.reducer";

export {
    actionBuyAll,
    actionAddCart,
    actionRemoveCart,
    appReducer,
    cvReducer,
    actionCv,
    actionCurrentProd,
    actionModal,
    actionAddFavorites,
    actionRemoveFavorite,
    cardsReducer,
    actionFetchCards,
}