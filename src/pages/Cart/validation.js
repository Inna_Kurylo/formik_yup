import * as yup from 'yup'

export const validationSchema = yup.object({
    name: yup
        .string('Enter your name')
        .required('Name is required')
        .min(2,'Name is too short'),
    secondName: yup
        .string('Enter your second name')
        .required('Second name is required'),
    age: yup
        .number('How old are you?')
        .required('Age is required'),
    address: yup
        .string('Enter your address')
        .required('Address is required'),
    phone: yup
        .string('Enter your phone')
        .required('Phone is required'),
})