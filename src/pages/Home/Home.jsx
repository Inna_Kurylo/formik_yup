import PropTypes from 'prop-types';
import AllCards from "../../components/AllCards";

function Home({cards, handlerCurrentProd, handlerModal, handlerFavorite, text}) {
  
    return(
    <div className="main">
            <AllCards text={text} data={cards} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} handlerFavorite={handlerFavorite} />
    </div>)
}
Home.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    text: PropTypes.string,
    cards: PropTypes.array
};
export default Home;