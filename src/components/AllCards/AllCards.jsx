import PropTypes from 'prop-types';

import Card from "./components/Card";
import './AllCards.scss'

function AllCards({handlerCurrentProd, handlerModal, data, handlerFavorite, text}){
    const mappedData = data.map((item, index) => <Card text={text} handlerFavorite={handlerFavorite} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} key={index} data={item} />)
    return(

            <div className="wrap">
                <div className="cards" >
                    {data && mappedData}
                </div>
            </div>
        
       
    )
}
AllCards.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    data: PropTypes.array,
};
export default AllCards;