import PropTypes from 'prop-types';
import {ReactComponent as Cart} from './components/icons/cart.svg';
import './Button.scss';


function Button({handlerClick, text}){
    return(
        <div className="wrapper">
                <button className ="button" type="button" onClick={handlerClick}>
                    <span className="button--text">
                    {text}
                    </span>
                    <Cart/>
                </button>
            </div>
    )
}

Button.propTypes = {
	handlerClick: PropTypes.func,
};
export default Button;